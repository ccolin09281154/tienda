/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entidad;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author shadow
 */
@Entity
@Table(name = "pedido")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pedido.findAll", query = "SELECT p FROM Pedido p"),
    @NamedQuery(name = "Pedido.findByNumeroPedido", query = "SELECT p FROM Pedido p WHERE p.numeroPedido = :numeroPedido"),
    @NamedQuery(name = "Pedido.findByDeposito", query = "SELECT p FROM Pedido p WHERE p.deposito = :deposito"),
    @NamedQuery(name = "Pedido.findBySubtotal", query = "SELECT p FROM Pedido p WHERE p.subtotal = :subtotal"),
    @NamedQuery(name = "Pedido.findByFecha", query = "SELECT p FROM Pedido p WHERE p.fecha = :fecha"),
    @NamedQuery(name = "Pedido.findByStatus", query = "SELECT p FROM Pedido p WHERE p.status = :status"),
    @NamedQuery(name = "Pedido.findByConfirmacionApartado", query = "SELECT p FROM Pedido p WHERE p.confirmacionApartado = :confirmacionApartado"),
    @NamedQuery(name = "Pedido.findByConfirmacionVenta", query = "SELECT p FROM Pedido p WHERE p.confirmacionVenta = :confirmacionVenta")})
public class Pedido implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "numero_pedido")
    private Integer numeroPedido;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "deposito")
    private BigDecimal deposito;
    @Basic(optional = false)
    @NotNull
    @Column(name = "subtotal")
    private BigDecimal subtotal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "status")
    private int status;
    @Basic(optional = false)
    @NotNull
    @Column(name = "confirmacion_apartado")
    private int confirmacionApartado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "confirmacion_venta")
    private int confirmacionVenta;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "numPedido")
    private Collection<Detallepedido> detallepedidoCollection;
    @JoinColumn(name = "id_cliente", referencedColumnName = "login")
    @ManyToOne(optional = false)
    private Cliente idCliente;
    @JoinColumn(name = "id_usuario", referencedColumnName = "login")
    @ManyToOne
    private Usuario idUsuario;

    public Pedido() {
    }

    public Pedido(Integer numeroPedido) {
        this.numeroPedido = numeroPedido;
    }

    public Pedido(Integer numeroPedido, BigDecimal deposito, BigDecimal subtotal, Date fecha, int status, int confirmacionApartado, int confirmacionVenta) {
        this.numeroPedido = numeroPedido;
        this.deposito = deposito;
        this.subtotal = subtotal;
        this.fecha = fecha;
        this.status = status;
        this.confirmacionApartado = confirmacionApartado;
        this.confirmacionVenta = confirmacionVenta;
    }

    public Integer getNumeroPedido() {
        return numeroPedido;
    }

    public void setNumeroPedido(Integer numeroPedido) {
        this.numeroPedido = numeroPedido;
    }

    public BigDecimal getDeposito() {
        return deposito;
    }

    public void setDeposito(BigDecimal deposito) {
        this.deposito = deposito;
    }

    public BigDecimal getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(BigDecimal subtotal) {
        this.subtotal = subtotal;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getConfirmacionApartado() {
        return confirmacionApartado;
    }

    public void setConfirmacionApartado(int confirmacionApartado) {
        this.confirmacionApartado = confirmacionApartado;
    }

    public int getConfirmacionVenta() {
        return confirmacionVenta;
    }

    public void setConfirmacionVenta(int confirmacionVenta) {
        this.confirmacionVenta = confirmacionVenta;
    }

    @XmlTransient
    public Collection<Detallepedido> getDetallepedidoCollection() {
        return detallepedidoCollection;
    }

    public void setDetallepedidoCollection(Collection<Detallepedido> detallepedidoCollection) {
        this.detallepedidoCollection = detallepedidoCollection;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Cliente idCliente) {
        this.idCliente = idCliente;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numeroPedido != null ? numeroPedido.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pedido)) {
            return false;
        }
        Pedido other = (Pedido) object;
        if ((this.numeroPedido == null && other.numeroPedido != null) || (this.numeroPedido != null && !this.numeroPedido.equals(other.numeroPedido))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.Pedido[ numeroPedido=" + numeroPedido + " ]";
    }
    
}
