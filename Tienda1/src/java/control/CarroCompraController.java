/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package control;


import static com.sun.faces.el.FacesCompositeELResolver.ELResolverChainType.Faces;
import entidad.Cliente;
import entidad.Libro;
import entidad.Pedido;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.event.DragDropEvent;

/**
 *
 * @author shadow
 */
@ManagedBean(name="carroCompra")
@SessionScoped
public class CarroCompraController implements Serializable{
    private Libro libro;
    private List<Libro> carro;
    private Itemcar itemcar;
    private List<Itemcar> carrocompra;
    private int cant=0;
    private int cantidad=0;
    private BigDecimal subtotal=new BigDecimal(0);
    private BigDecimal subt=new BigDecimal(0);
    private BigDecimal total=new BigDecimal(0);
    private BigDecimal dep=new BigDecimal(0);
    private Pedido pedido;
    private Cliente cliente=new Cliente();
    @ManagedProperty(value="#{clienteBean}")
    private ClienteController clienteBean;
    @ManagedProperty(value="#{PedidoBean}")
    private PedidoController pedidoBean;
    @EJB
    private stateless.ClienteFacade clienteFacade;
    @EJB
    private stateless.PedidoFacade pedidoFacade;
    private Date fechapedido;
    private int numaut=0;
    private int nump=0;
    @PostConstruct
    public void init(){
        carrocompra=new ArrayList<Itemcar>();
        carro=new ArrayList<Libro>();
    }
    
    /**
     * Creates a new instance of CarroCompra
     */
    public CarroCompraController() {
    }
    
    public void addPedido(){
        numaut++;
        clienteFacade.save(cliente);
        fechapedido=new Date();
        pedido=new Pedido();
        pedido.setConfirmacionApartado(numaut);
        pedido.setDeposito(dep);
        pedido.setFecha(fechapedido);
        pedido.setIdCliente(cliente);
        pedido.setStatus(0);
        pedido.setSubtotal(total);
        pedidoFacade.save(pedido);
        nump=pedido.getNumeroPedido();
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("Ticket.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(CarroCompraController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void sesionClear(){
        try {
            FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
            FacesContext.getCurrentInstance().getExternalContext().redirect("../index.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(CarroCompraController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void addCarro(Libro lib){
        libro=lib;
        if(carrocompra.isEmpty()){
            itemcar=new Itemcar();
            itemcar.setLibro(lib);
            cant++;
            subtotal=libro.getPrecio();
            itemcar.setCantidad(1);
            itemcar.setTotal(subtotal);
            carrocompra.add(itemcar);
            calcCantidadTotal();
            calcSubtotal();;
            System.out.println("primer if");
            //calcSubtotal();
        }else{
            boolean libexist=false;
            int x=0;
            System.out.println("elsef");
            for(int i=0;i<carrocompra.size();i++){
                if(carrocompra.get(i).getLibro().getIsbn().compareTo(libro.getIsbn())==0){
                    libexist=true;
                    x=i;
                    System.out.println("for");
                    //cantidad++;
                }else{
                    libexist=false;
                    System.out.println("else dentro de else");
                }
            }
            if(libexist){
                carrocompra.get(x).setCantidad(carrocompra.get(x).getCantidad()+1);
                System.out.println("");
                calcCantidadTotal();
                calcSubtotal();
            }else{
                itemcar=new Itemcar();
                itemcar.setLibro(lib);
                itemcar.setCantidad(1);
                itemcar.setTotal(libro.getPrecio());
                carrocompra.add(itemcar);
                calcCantidadTotal();
                calcSubtotal();
            }
        }
       
    }
    public void agregarCarro(Libro lib){
        System.out.println("size "+carro.size());
        //System.out.println("libro "+libro);
        libro=lib;
        if(carro.isEmpty()){
            System.out.println("empty");
            carro.add(libro);
            total=libro.getPrecio();
            cantidad++;
        }else{
            System.out.println("libro "+libro);
            boolean libexist=false;
            int x=0;
            for(int i=0;i<carro.size();i++){
                if(carro.get(i).getIsbn().compareTo(libro.getIsbn())==0){
                    libexist=true;
                    x=i;
                }else{
                    libexist=false;
                }
            }
            if(libexist){
                cantidad++;
                
                BigDecimal t= total.add(libro.getPrecio());
                total=t;
            }else{
                carro.add(libro);
            }
            //cantidad++;
            //BigDecimal t= total.add(libro.getPrecio());
            //total=t;
            //carro.add(libro);
        }
    }
    
    public void removeCarro(Libro lib){
        libro=lib;
        System.out.println(""+lib.getIsbn());
        for (int i = 0; i < carrocompra.size(); i++) {
            if(carrocompra.get(i).getLibro().getIsbn().compareTo(libro.getIsbn())==0){
                if(carrocompra.get(i).getCantidad()>1){
                    carrocompra.get(i).setCantidad(carrocompra.get(i).getCantidad()-1);
                    calcSubtotal();
                    calcCantidadTotal();
                }else{
                    carrocompra.remove(i);
                    calcSubtotal();
                    calcCantidadTotal();
                }
            }
        }
         
    }
    
    public void calcSubtotal(){
        total=new BigDecimal(0);
        for (int i = 0; i < carrocompra.size(); i++) {
            total=total.add(carrocompra.get(i).getLibro().getPrecio().multiply(new BigDecimal(carrocompra.get(i).getCantidad())));
        }
        
    }
    public void calcCantidadTotal(){
        cantidad=0;
        for (int i = 0; i < carrocompra.size(); i++) {
            cantidad+=carrocompra.get(i).getCantidad();
        }
    }
    
    public void calculaDeposito(){
        BigDecimal porcent=new BigDecimal(0.20);
        dep=total.multiply(porcent);
        dep = dep.setScale(2, RoundingMode.CEILING);
    }
    
    public void pagar(){
        calculaDeposito();
        if(carrocompra.isEmpty()){
            try {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info" ,"Tu carro de compra esta vacio ") );
                FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(CarroCompraController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("cliente/login.xhtml");
                //FacesContext.getCurrentInstance().getExternalContext().redirect("libro/RegistroCliente.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(CarroCompraController.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }
    
    public void onLibroDrop(DragDropEvent ddEvent) {
        Libro lib = ((Libro) ddEvent.getData());
        addCarro(lib);
    }
    
    public void regresar(){
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("../index.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(CarroCompraController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void cleanCarro(){
        carrocompra.clear();
        calcSubtotal();
        calcCantidadTotal();
        System.out.println(""+carrocompra.size());
    }
    /**
     * @return the libro
     */
    public Libro getLibro() {
        return libro;
    }

    /**
     * @param libro the libro to set
     */
    public void setLibro(Libro libro) {
        this.libro = libro;
    }

    /**
     * @return the carro
     */
    public List<Libro> getCarro() {
        return carro;
    }

    /**
     * @param carro the carro to set
     */
    public void setCarro(List<Libro> carro) {
        this.carro = carro;
    }

    /**
     * @return the cantidad
     */
    public int getCantidad() {
        return cantidad;
    }

    /**
     * @param cantidad the cantidad to set
     */
    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    /**
     * @return the total
     */
    public BigDecimal getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    /**
     * @return the cant
     */
    public int getCant() {
        return cant;
    }

    /**
     * @param cant the cant to set
     */
    public void setCant(int cant) {
        this.cant = cant;
    }

    /**
     * @return the itemcar
     */
    public Itemcar getItemcar() {
        return itemcar;
    }

    /**
     * @param itemcar the itemcar to set
     */
    public void setItemcar(Itemcar itemcar) {
        this.itemcar = itemcar;
    }

    /**
     * @return the carrocompra
     */
    public List<Itemcar> getCarrocompra() {
        return carrocompra;
    }

    /**
     * @param carrocompra the carrocompra to set
     */
    public void setCarrocompra(List<Itemcar> carrocompra) {
        this.carrocompra = carrocompra;
    }

    /**
     * @param subtotal the subtotal to set
     */
    public void setSubtotal(BigDecimal subtotal) {
        this.subtotal = subtotal;
    }

    /**
     * @return the subtotal
     */
    public BigDecimal getSubtotal() {
        return subtotal;
    }

    /**
     * @return the dep
     */
    public BigDecimal getDep() {
        return dep;
    }

    /**
     * @param dep the dep to set
     */
    public void setDep(BigDecimal dep) {
        this.dep = dep;
    }

    /**
     * @return the clienteBean
     */
    public ClienteController getClienteBean() {
        return clienteBean;
    }

    /**
     * @param clienteBean the clienteBean to set
     */
    public void setClienteBean(ClienteController clienteBean) {
        this.clienteBean = clienteBean;
    }

    /**
     * @return the clienteFacade
     */
    public stateless.ClienteFacade getClienteFacade() {
        return clienteFacade;
    }

    /**
     * @param clienteFacade the clienteFacade to set
     */
    public void setClienteFacade(stateless.ClienteFacade clienteFacade) {
        this.clienteFacade = clienteFacade;
    }

    /**
     * @return the pedidoBean
     */
    public PedidoController getPedidoBean() {
        return pedidoBean;
    }

    /**
     * @param pedidoBean the pedidoBean to set
     */
    public void setPedidoBean(PedidoController pedidoBean) {
        this.pedidoBean = pedidoBean;
    }

    /**
     * @return the pedidoFacade
     */
    public stateless.PedidoFacade getPedidoFacade() {
        return pedidoFacade;
    }

    /**
     * @param pedidoFacade the pedidoFacade to set
     */
    public void setPedidoFacade(stateless.PedidoFacade pedidoFacade) {
        this.pedidoFacade = pedidoFacade;
    }

    /**
     * @return the pedido
     */
    public Pedido getPedido() {
        return pedido;
    }

    /**
     * @param pedido the pedido to set
     */
    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    /**
     * @return the cliente
     */
    public Cliente getCliente() {
        return cliente;
    }

    /**
     * @param cliente the cliente to set
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    /**
     * @return the fechapedido
     */
    public Date getFechapedido() {
        return fechapedido;
    }

    /**
     * @param fechapedido the fechapedido to set
     */
    public void setFechapedido(Date fechapedido) {
        this.fechapedido = fechapedido;
    }

    /**
     * @return the numaut
     */
    public int getNumaut() {
        return numaut;
    }

    /**
     * @param numaut the numaut to set
     */
    public void setNumaut(int numaut) {
        this.numaut = numaut;
    }

    /**
     * @return the nump
     */
    public int getNump() {
        return nump;
    }

    /**
     * @param nump the nump to set
     */
    public void setNump(int nump) {
        this.nump = nump;
    }
    

}
