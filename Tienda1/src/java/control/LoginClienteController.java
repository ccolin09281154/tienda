/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package control;

import entidad.Cliente;
import entidad.Usuario;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author shadow
 */
@ManagedBean(name="loginCliente")
@SessionScoped
public class LoginClienteController implements Serializable{
    
    private String mensaje;
    private Cliente cliente;
    @ManagedProperty(value="#{clienteBean}")
    private ClienteController clienteBean;
    @EJB
    private stateless.ClienteFacade ejbFacade;
    /**
     * Creates a new instance of LoginClienteController
     */
    public LoginClienteController() {
        cliente=new Cliente();
    }
    
    public void autenticar(){
       Cliente cli=getEjbFacade().findByLogin(cliente.getLogin(), cliente.getPassword());
       if(cli!=null){
           try {
               FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario", cli);
               FacesContext.getCurrentInstance().getExternalContext().redirect("PagoCliente.xhtml");
           } catch (IOException ex) {
               System.out.println("Error en la autenticacion");
           }
       }else{
           String errorMessage="Usuario y/o contraseña incorrectos";
           FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                   errorMessage, errorMessage);
           FacesContext.getCurrentInstance().addMessage("form:message", message);
       }
    }
    
    public void logout(){
        try {
            FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
            FacesContext.getCurrentInstance().getExternalContext().redirect("../index.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(LoginClienteController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void registrarse(){
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("../libro/RegistroCliente.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /**
     * @return the cliente
     */
    public Cliente getCliente() {
        if(clienteBean!=null){
            cliente=clienteBean.getSelected();
        }
        return cliente;
    }

    /**
     * @param cliente the cliente to set
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    /**
     * @return the clienteBean
     */
    public ClienteController getClienteBean() {
        return clienteBean;
    }

    /**
     * @param clienteBean the clienteBean to set
     */
    public void setClienteBean(ClienteController clienteBean) {
        this.clienteBean = clienteBean;
    }

    /**
     * @return the ejbFacade
     */
    public stateless.ClienteFacade getEjbFacade() {
        return ejbFacade;
    }

    /**
     * @param ejbFacade the ejbFacade to set
     */
    public void setEjbFacade(stateless.ClienteFacade ejbFacade) {
        this.ejbFacade = ejbFacade;
    }
    

}
