/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package control;

import entidad.Usuario;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author shadow
 */
@ManagedBean(name="login")
@SessionScoped
public class LoginController implements Serializable{

    private String mensaje;
    private Usuario usuario;
    @EJB
    private stateless.UsuarioFacade ejbFacade;
    
    /**
     * Creates a new instance of Login
     */
    public LoginController() {
        usuario=new Usuario();
    }
    
    public void autenticar(){
       Usuario user=ejbFacade.findByLogin(usuario.getLogin(), usuario.getPwd());
       if(user!=null){
           try {
               FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario", usuario);
               FacesContext.getCurrentInstance().getExternalContext().redirect("sesioniniciada.xhtml");
           } catch (IOException ex) {
               System.out.println("Error en la autenticacion");
           }
       }else{
           String errorMessage="Usuario y/o contraseña incorrectos";
           FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                   errorMessage, errorMessage);
           FacesContext.getCurrentInstance().addMessage("form:message", message);
       }
    }
    
    public String logout(){
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        
        return "login.xhtml";
    }


    /**
     * @return the usuario
     */
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the ejbFacade
     */
    public stateless.UsuarioFacade getEjbFacade() {
        return ejbFacade;
    }

    /**
     * @param ejbFacade the ejbFacade to set
     */
    public void setEjbFacade(stateless.UsuarioFacade ejbFacade) {
        this.ejbFacade = ejbFacade;
    }

    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
    
    

    
}
