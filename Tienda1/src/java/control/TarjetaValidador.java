/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package control;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author shadow
 */
@FacesValidator(value="cardvalidator")
public class TarjetaValidador implements Validator
{
    private String msj;
    
    public String procesarCard(String tnum){
        String ccnum=null;
        ccnum=tnum.replace("-", "");
        return ccnum;
    }
    
    public boolean checaTarjeta(String ccnum){
        int sum=0;
        for(int i=ccnum.length()-1; i >= 0; i -= 2) {
         sum += Integer.parseInt(ccnum.substring(i, i + 1));
         if(i > 0) {
            int d = 2 * Integer.parseInt(ccnum.substring(i - 1, i));
            if(d > 9) d -= 9;
            sum += d;
         }
      }
     return sum % 10 == 0;  
    }

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String cardNumber=String.valueOf(value);
        boolean valid = true;
        cardNumber=procesarCard(cardNumber);
        if (value  == null) {
            valid=false;  
        }else if (!checaTarjeta(cardNumber)) {
            valid = false;
        }
        if(!valid){
            msj="invalido";
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!" ,"Número de tarjeta " + msj) );
            throw new ValidatorException(context.getMessageList());
        }else{
            msj="valido";
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info" ,"Número de tarjeta " + msj) );
            
        }
    }

    /**
     * @return the msj
     */
    public String getMsj() {
        return msj;
    }

    /**
     * @param msj the msj to set
     */
    public void setMsj(String msj) {
        this.msj = msj;
    }
    
}
