/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package control;

import entidad.Autor;
import entidad.Libro;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import stateless.AutorFacade;
import stateless.LibroFacade;

/**
 *
 * @author shadow
 */
@ManagedBean
@SessionScoped
public class BuscaLibroController implements Serializable{
    private String text;
    private String opcion;
    private String busqueda;
    private List<String> opciones;  
    private List<Libro> libros;
    private List<Libro> carro;
    private Libro libro;
    private int cantidad=0;
    private BigDecimal total=new BigDecimal(0);
    @EJB
    private LibroFacade ejbFacade;
    @EJB
    private AutorFacade ejbAutorFacade;
    
    @PostConstruct
    public void init(){
        carro=new ArrayList<Libro>();
        opciones=new ArrayList<String>();
        opciones.add("ISBN");
        opciones.add("Titulo");
        opciones.add("Autor");
        opciones.add("Categoria");
        
        
    }
    /**
     * Creates a new instance of BuscaLibroController
     */
    public BuscaLibroController() {
        libro=new Libro();
    }
    
    public void buscaISBN(){
        libros=new ArrayList<Libro>();
        Libro lib=ejbFacade.findByISBN(this.busqueda);
        if(lib!=null){
            libro=lib;
            libros.add(libro);
            System.out.println("se encontro un libro "+libro.getTitulo());
            //text="123456";
        }else{
            text="No se encontro ningun libro";
        }
    }
    public void buscaTitulo(){
        libros=new ArrayList<Libro>();
        List<Libro> list=ejbFacade.findByTitulo(this.busqueda);
        if(list!=null){
            libros=list;
            System.out.println(""+list.isEmpty());
        }
    }
    
    public void buscaAutor(){
        libros=new ArrayList<Libro>();
        List<Libro> list=ejbFacade.finWhitAutor(this.busqueda);
        if(list!=null){
            libros=list;
            System.out.println(""+list.isEmpty());
        }
    }
    
    public void buscaCategoria(){
        libros=new ArrayList<Libro>();
        List<Libro> list=ejbFacade.findWhitCategoria(this.busqueda);
        if(list!=null){
            libros=list;
            System.out.println(""+list.isEmpty());
        }
    }

    public void buscarLibro()
    {
        if(opcion.compareTo("ISBN")==0){
           buscaISBN();
            for (int i = 0; i < libros.size(); i++){
                System.out.println("i "+i);
                System.out.println(">>"+libros.get(i).getTitulo());
            }
        }else if(opcion.compareTo("Titulo")==0){
          buscaTitulo();
        }else if(opcion.compareTo("Autor")==0){
          buscaAutor();
        }else if(opcion.compareTo("Categoria")==0){
          buscaCategoria();
        }
    }
    
    public void agregarCarro(Libro lib){
        System.out.println("size "+carro.size());
        //System.out.println("libro "+libro);
        libro=lib;
        if(carro.isEmpty()){
            System.out.println("empyt");
            carro.add(libro);
            total=libro.getPrecio();
            cantidad++;
        }else{
            System.out.println("libro "+libro);
            for(int i=0;i<carro.size();i++){
                if(carro.get(i).getIsbn().compareTo(libro.getIsbn())==0){
                    cantidad++;
                }else{
                    carro.add(libro);
                }
            }
            cantidad++;
            BigDecimal t= total.add(libro.getPrecio());
            total=t;
            //carro.add(libro);
        }
    }
    
    /**
     * @return the libro
     */
    public Libro getLibro() {
        return libro;
    }

    /**
     * @param libro the libro to set
     */
    public void setLibro(Libro libro) {
        this.libro = libro;
    }

    /**
     * @return the ejbFacade
     */
    public LibroFacade getEjbFacade() {
        return ejbFacade;
    }

    /**
     * @param ejbFacade the ejbFacade to set
     */
    public void setEjbFacade(LibroFacade ejbFacade) {
        this.ejbFacade = ejbFacade;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @return the opcion
     */
    public String getOpcion() {
        return opcion;
    }

    /**
     * @param opcion the opcion to set
     */
    public void setOpcion(String opcion) {
        this.opcion = opcion;
    }

    /**
     * @return the opciones
     */
    public List<String> getOpciones() {
        return opciones;
    }

    /**
     * @return the libros
     */
    public List<Libro> getLibros() {
        return libros;
    }

    /**
     * @param libros the libros to set
     */
    public void setLibros(List<Libro> libros) {
        this.libros = libros;
    }

    /**
     * @return the busqueda
     */
    public String getBusqueda() {
        return busqueda;
    }

    /**
     * @param busqueda the busqueda to set
     */
    public void setBusqueda(String busqueda) {
        this.busqueda = busqueda;
    }

    /**
     * @return the carro
     */
    public List<Libro> getCarro() {
        return carro;
    }

    /**
     * @param carro the carro to set
     */
    public void setCarro(List<Libro> carro) {
        this.carro = carro;
    }

    /**
     * @return the cantidad
     */
    public int getCantidad() {
        return cantidad;
    }

    /**
     * @param cantidad the cantidad to set
     */
    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    /**
     * @return the total
     */
    public BigDecimal getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(BigDecimal total) {
        this.total = total;
    }
    
}
