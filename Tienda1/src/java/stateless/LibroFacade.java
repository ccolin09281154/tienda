/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stateless;

import entidad.Autor;
import entidad.Autorlibro;
import entidad.Categoria;
import entidad.Libro;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author shadow
 */
@Stateless
public class LibroFacade extends AbstractFacade<Libro> {
    @PersistenceContext(unitName = "Tienda1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LibroFacade() {
        super(Libro.class);
    }
    public Libro findByISBN(String isbn){
        Libro lib=null;
        if(isbn!=null){
            lib=em.find(Libro.class, isbn);
        }
        return lib;
    }
    
    public List<Libro> findByTitulo(String titulo){
        List<Libro> list=null;
        if(titulo!=null){
            Query q=em.createQuery("SELECT l FROM Libro l WHERE l.titulo LIKE :titulo");
            q.setParameter("titulo", '%' + titulo + '%');
            list=q.getResultList();
        }
        return list;
    }
    
    public List<Libro> finWhitAutor(String autor){
        List<Libro> list=null;
        if(autor!=null){
            Query q=em.createQuery("SELECT a FROM Autor a WHERE a.nombres LIKE :nombres OR a.apellidos LIKE :apellidos");
            q.setParameter("nombres", '%' + autor + '%');
            q.setParameter("apellidos", '%' + autor + '%');
            Query q2=em.createNamedQuery("Autorlibro.findAll");
            List<Autorlibro> autlibrolist=q2.getResultList();
            List<Autor> autorlist=q.getResultList();
            System.out.println("size al "+autlibrolist.get(0));
            System.out.println("size a "+autorlist.size());
            list=new ArrayList<Libro>();
            for (int i = 0; i <autlibrolist.size(); i++) {
                for (int j = 0; j < autorlist.size(); j++) {
                    if(autlibrolist.get(i).getIdAutor().getIdAutor()==autorlist.get(j).getIdAutor()){
                        list.add(autlibrolist.get(i).getIdLibro());
                    }
                }
            }
        }
        return list;
    }
    
    public List<Libro> findWhitCategoria(String categoria){
        List<Libro> list=null;
        if(categoria!=null){
            Query q=em.createQuery("SELECT c FROM Categoria c WHERE c.nombre LIKE :nombre");
            q.setParameter("nombre", '%' + categoria + '%');
            Query q2=em.createNamedQuery("Libro.findAll");
            List<Libro> liblist=q2.getResultList();
            List<Categoria> catlist=q.getResultList();
            list=new ArrayList<Libro>();
            for (int i = 0; i < catlist.size(); i++) {
                for (int j = 0; j < liblist.size(); j++) {
                    if(catlist.get(i).getIdCategoria()==liblist.get(j).getIdCategoria().getIdCategoria()){
                        list.add(liblist.get(j));
                    }
                }
            }
        }
        return list;
    }  
}
