/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stateless;

import entidad.Cliente;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author shadow
 */
@Stateless
public class ClienteFacade extends AbstractFacade<Cliente> {
    @PersistenceContext(unitName = "Tienda1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ClienteFacade() {
        super(Cliente.class);
    }
    public Cliente findByLogin(String login, String password){
        Cliente cliente=null;
        if(login!=null){
            Cliente cli=em.find(Cliente.class, login);
            if(cli!=null){
                if(cli.getPassword().compareTo(password)==0){
                    cliente=cli;
                }
            }
        }
        return cliente;
    }
    
    public void save(Cliente cliente){
        if(cliente!=null){
            em.persist(cliente);
            em.flush();
        }
    }
}
