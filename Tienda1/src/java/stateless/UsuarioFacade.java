/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stateless;

import entidad.Usuario;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author shadow
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> {
    @PersistenceContext(unitName = "Tienda1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }
    
    public Usuario findByLogin(String login, String password){
        Usuario user=null;
        if(login!=null){
            Usuario useraux=em.find(Usuario.class, login);
            if(useraux!=null){
                if(useraux.getPwd().compareTo(password)==0){
                    user=useraux;
                }
            }
        }
        return user;
    }
    
}
